package com.gitlab.soshibby.issuereporter.messageappenders.kibana;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

@JsonIgnoreProperties("class")
public class Config {
    private static ObjectMapper mapper = new ObjectMapper();
    private String linkText;
    private String url;
    private String index;
    private List<String> columns;

    public String getLinkText() {
        return linkText;
    }

    public void setLinkText(String linkText) {
        this.linkText = linkText;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public static Config from(String config) {
        try {
            return mapper.readValue(config, Config.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

