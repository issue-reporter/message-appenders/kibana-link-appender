package com.gitlab.soshibby.issuereporter.messageappenders.kibana;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.messageappender.MessageAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class KibanaLinkAppender implements MessageAppender {

    private static final Logger log = LoggerFactory.getLogger(KibanaLinkAppender.class);
    private Config configuration;
    private SimpleDateFormat format;

    @Override
    public void init(String config) {
        configuration = Config.from(config);
        validateConfiguration(configuration);

        format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Override
    public String createMessageFrom(LogStatement logTrigger, List<LogStatement> logStatements) {
        log.info("Generating Kibana link for trace id {}.", logTrigger.getTraceId());

        List<LogStatement> sortedLogs = logStatements.stream()
                .sorted(Comparator.comparing(LogStatement::getDate))
                .collect(Collectors.toList());

        Date startDate = sortedLogs.get(0).getDate();
        Date endDate = sortedLogs.size() == 1 ? sortedLogs.get(0).getDate() : sortedLogs.get(sortedLogs.size() - 1).getDate();

        List<String> columns = configuration.getColumns() != null ? configuration.getColumns() : new ArrayList<>();

        return new StringBuilder()
                .append("[")
                .append(configuration.getLinkText())
                .append("]")
                .append("(")
                .append(configuration.getUrl())
                .append("/app/kibana#/discover?_g=(refreshInterval:(pause:!t,value:0),time:(from:'")
                .append(format.format(startDate))
                .append("',mode:absolute,to:'")
                .append(format.format(endDate))
                .append("'))&_a=(columns:!(" + String.join(",", columns) + "),filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'")
                .append(configuration.getIndex())
                .append("',key:trace_id,negate:!f,params:(query:'")
                .append(logTrigger.getTraceId())
                .append("',type:phrase),type:phrase,value:'")
                .append(logTrigger.getTraceId())
                .append("'),query:(match:(trace_id:(query:'")
                .append(logTrigger.getTraceId())
                .append("',type:phrase))))),index:'")
                .append(configuration.getIndex())
                .append("',interval:auto,query:(language:lucene,query:''),sort:!('@timestamp',desc))")
                .append(")")
                .toString();
    }

    private void validateConfiguration(Config config) {
        Assert.notNull(config, "Configuration is null.");
        Assert.hasText(config.getLinkText(), "Link text is null or empty in config.");
        Assert.hasText(config.getUrl(), "Url is null or empty in config.");
    }

}

